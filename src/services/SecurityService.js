import Oidc from 'oidc-client';
import Vue from 'vue';

let mgr = new Oidc.UserManager({
    userStore: new Oidc.WebStorageStateStore(),
    authority: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_SMUKbYm1l/.well-known/openid-configuration',
    client_id: '7mfgevi4i9e9asllk7polebunq',
    redirect_uri: window.location.origin + '/static/callback.html',
    response_type: 'code',
    scope: 'openid profile phone email',
    metadata: {
        issuer: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_SMUKbYm1l',
        authorization_endpoint: 'https://dayebensa.auth.us-east-2.amazoncognito.com/oauth2/authorize',
        userinfo_endpoint:'https://dayebensa.auth.us-east-2.amazoncognito.com/oauth2/userinfo',
        end_session_endpoint: 'https://dayebensa.auth.us-east-2.amazoncognito.com/logout',
        jwks_uri:'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_SMUKbYm1l/.well-known/jwks.json'
    }
});

mgr.events.addAccessTokenExpiring(function () {
    Vue.$log.debug('AccessToken Expiring：', arguments);
});


export default class SecurityService {

    renewToken () {
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.signinSilent().then(function (user) {
                if (user == null) {
                    self.signIn(null);
                } else{
                    return resolve(user);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Get the user who is logged in
    getUser () {
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Check if there is any user logged in
    getSignedIn () {
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(false);
                } else{
                    return resolve(true);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Redirect of the current window to the authorization endpoint.
    signIn () {
        mgr.signinRedirect();
    }

    processSignInCallback() {
        return mgr.signinRedirectCallback();
    }

    // Redirect of the current window to the end session endpoint
    signOut () {
        mgr.signoutRedirect().then(function (resp) {
            Vue.$log.info('signed out', resp);
        }).catch(function (err) {
            Vue.$log.error(err);
        });
    }

    // Get the profile of the user logged in
    getProfile () {
        let self = this
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user.profile);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Get the token id
    getIdToken(){
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user.id_token);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Get the session state
    getSessionState(){
        let self = this
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user.session_state);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Get the access token of the logged in user
    getAcessToken(){
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user.access_token);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Takes the scopes of the logged in user
    getScopes(){
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user.scopes);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }

    // Get the user roles logged in
    getRole () {
        let self = this;
        return new Promise((resolve, reject) => {
            mgr.getUser().then(function (user) {
                if (user == null) {
                    self.signIn();
                    return resolve(null);
                } else{
                    return resolve(user.profile.role);
                }
            }).catch(function (err) {
                Vue.$log.error(err);
                return reject(err);
            });
        });
    }
}