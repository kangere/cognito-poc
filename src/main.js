import Vue from 'vue';
import App from './App.vue';
import VueLogger from 'vuejs-logger';
import router from "./routes";
import store from './store'


const isProduction = process.env.NODE_ENV === 'production';

Vue.config.productionTip = false

const options = {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
};


Vue.use(VueLogger, options);


new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app');






