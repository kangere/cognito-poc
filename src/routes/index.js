import OidcCallback from '../views/OidcCallback'
import VueRouter from "vue-router";
import Vue from 'vue';
import WelcomePage from "../components/WelcomePage";
import HelloWorld from "../components/HelloWorld";
import OidcCallbackError from "../views/OidcCallbackError";
import OidcLogoutCallback from "../views/OidcLogoutCallback";
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc'
import store from '@/store'


Vue.use(VueRouter);


const routes = [
    {
        path: '/',
        name: 'home',
        component: HelloWorld,
        meta: {
            isPublic: true
        }
    },
    {
        path: '/protected',
        name: 'Welcome',
        component: WelcomePage
    },
    {
        path: '/oidc-callback', // Needs to match redirectUri (redirect_uri if you use snake case) in you oidcSettings
        name: 'oidcCallback',
        component: OidcCallback
    },
    {
        path: '/logout',
        name: 'oicdLogoutCallback',
        component: OidcLogoutCallback
    },
    {
        path: '/oidc-callback-error', // Needs to match redirect_uri in you oidcSettings
        name: 'oidcCallbackError',
        component: OidcCallbackError,
        meta: {
            isPublic: true
        }
    }
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});
router.beforeEach(vuexOidcCreateRouterMiddleware(store,'oidcStore'));

export default router